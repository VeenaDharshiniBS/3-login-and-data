const fs = require("fs");
const { promisify } = require("util");

const promiseReadFile = promisify(fs.readFile);
const promiseWriteFile = promisify(fs.writeFile);
const promiseDeleteFile = promisify(fs.unlink);
const promiseAppendFile = promisify(fs.appendFile);

function problem1(files) {
    Promise.all(files.map(file=>promiseWriteFile(file,`Inside ${file}`)))
        .then(() => {
            console.log("Files created");
            return new Promise(resolve => setTimeout(resolve, 2000));
        })
        .then(()=>promiseDeleteFile(files[0]))
        .then(()=>{
            console.log(`File1.txt is deleted`);
            return promiseDeleteFile(files[1]);
        })
        .then(()=>console.log(`File2.txt is deleted`))
        .catch((err) => console.error(err));
}
//problem1(['file1.txt', 'file2.txt']);

function problem2(fileName)
{
    promiseReadFile('lipsum.txt')
    .then((data)=>{
        console.log("File read");
        return promiseWriteFile(fileName,data)
    })
    .then(()=>{
        console.log("Written");
        return promiseDeleteFile('lipsum.txt');
    })
    .then(()=>console.log('original file deleted'))
    .catch((err)=>console.error(err));
}

//problem2('copyFile.txt');

function login(user, val) {
    if(val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1, 
            name: "Test",
        },
        {
            id: 2, 
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    
    let detail = `${activity} of user: ${user} at ${new Date()} \n`;
    fs.appendFile('login.txt',detail,'utf-8',(err)=>{
        if(err)
            console.error(err);
        else
            console.log("Log data successfully");
    })
}

function problem3(user,val)
{
    login(user,val)
    .then((res)=>{
        logData(user,"Login Success");
        console.log(res);
        getData()
        .then((res)=>{
            logData(user,"GetData Success");
            console.log(res);
        })
        .catch((err)=>{
            logData(user,"GetData Failure");
            console.error(err);
        })
    })
    .catch((err)=>{
        logData(user,"Login Failure");
        console.error(err);
    })
}

//problem3("Veena",2)